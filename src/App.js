import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Login, MainPage, PageNotFound } from './pages'

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/" component={MainPage} />
        <Route component={PageNotFound} />
      </Switch>
    </Router>
  );
}

export default App;