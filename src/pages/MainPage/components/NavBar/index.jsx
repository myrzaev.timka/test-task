import { AppBar, Toolbar, Typography, MenuItem } from '@material-ui/core';
import { AccessAlarms, AccountBalance } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { Link, useHistory } from 'react-router-dom'

const useStyles = makeStyles(() => ({
    root: {
      flexGrow: 1,
      minWidth: 270,
      maxWidth: 270,
      minHeight: '100vh',
    },
    toolBar: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    appBar: {
        background: 'white',
        color: 'black',
        position: 'fixed',
        top: 0,
        left: 0,
        width: 270,
        height: '100%'
    },
    title: {
      flexGrow: 1,
    },
    icon: {
        marginRight: '10px'
    },
    link: {
        margin: '10px 0',
        width: '100%'
    }
}));

function NavBar() {
    const classes = useStyles();
    const history = useHistory()

    const randomizer = () => {
        const correctAnswer = localStorage.getItem('correctAnswer')
        if (correctAnswer) {
            history.push('/')
        } else {
            const firstRandomNumber = Math.floor(Math.random() * 100)
            const secRandomNumber = Math.floor(Math.random() * 100)
            const result = prompt(`Сколько будет ${firstRandomNumber + ' + ' + secRandomNumber}?`)
            if (Number(result) === firstRandomNumber + secRandomNumber) {
                history.push('/')
                localStorage.setItem('correctAnswer', result)
            } else {
                alert('Ошибка')
            }
        }
    }

    return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.appBar}>
                <Toolbar className={classes.toolBar}>
                    <Typography variant="h6" className={classes.title}>
                        Aibomed
                    </Typography>
                    <MenuItem className={classes.link} onClick={() => randomizer()}>
                        <AccessAlarms className={classes.icon} />
                        Главная
                    </MenuItem>
                    <MenuItem className={classes.link} component={Link} to="/tasks">
                        <AccountBalance className={classes.icon} />
                        Задачи
                    </MenuItem>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default NavBar