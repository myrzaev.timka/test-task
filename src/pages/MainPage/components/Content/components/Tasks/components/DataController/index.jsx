import { Box, Button, TextField } from '@material-ui/core';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    block: {
        width: '100%', 
        boxSizing: 'border-box',
        background: 'white',
        borderRadius: '4px',
        marginBottom: '30px',
        padding: '10px',
        display: 'flex',
        alignItems: 'center'
    },
    input: {
        width: 280
    },
    indent: {
        margin: '0 10px'
    }
}))

function NavBar({ searchValue, setSearchValue, fromDate, setFromDate, toDate, setToDate }) {
    const classes = useStyles();

    const throwOffFilter = () => {
        setSearchValue('')
        setFromDate(null)
        setToDate(null)
    }

    return (
        <Box className={classes.block}>
            <TextField
                id="search" 
                label="Поиск по наименованию" 
                variant="outlined" 
                className={classes.input}
                size="small"
                value={searchValue}
                onChange={e => setSearchValue(e.target.value)}
            />
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                    autoOk
                    variant="inline"
                    inputVariant="outlined"
                    label="Период от"
                    size="small"
                    format="dd.MM.yyyy"
                    value={fromDate} 
                    onChange={setFromDate}
                    className={classes.indent}
                />
                <KeyboardDatePicker
                    autoOk
                    variant="inline"
                    inputVariant="outlined"
                    label="Период до"
                    size="small"
                    format="dd.MM.yyyy"
                    value={toDate} 
                    onChange={setToDate} 
                    className={classes.indent}
                />
            </MuiPickersUtilsProvider>
            <Button variant="outlined" color="primary" onClick={throwOffFilter}>Сбросить</Button>
        </Box>
    )
}

export default NavBar