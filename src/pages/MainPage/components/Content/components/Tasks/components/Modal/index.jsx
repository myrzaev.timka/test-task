import { useState, memo } from 'react'
import CloseIcon from '@material-ui/icons/Close';
import MenuComponent from './MenuComponent'
import DateFnsUtils from '@date-io/date-fns'
import { makeStyles } from '@material-ui/core/styles';
import { Modal, Box, Typography, TextField, Button } from '@material-ui/core';
import { KeyboardDateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { QueryClientProvider, useMutation, useQuery } from 'react-query'
import { useForm, Controller } from "react-hook-form"
import { assignee } from '../../../../../../../../api/endpoints'
import { createTask, readTask, doneTask, saveTask } from '../../../../../../../../api/privatAPI'
import { useEffect } from 'react';
import { ToastsStore } from 'react-toasts';

const useStyles = makeStyles((theme) => ({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    content: {
      backgroundColor: 'white',
      boxShadow: theme.shadows[5],
      width: '60%'
    },
    titleBlock: {
        borderBottom: '1px solid lightgray',
        padding: '10px 20px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    closeIcon: {
        cursor: 'pointer'
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        padding: '10px 20px'
    },
    actionsBlock: {
        borderTop: '1px solid lightgray',
        padding: '10px 20px',
        marginTop: '80px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    input: {
        width: '100%',
        margin: '10px 0'
    }
  }));

function App({ uuid, setUuid, isOpen, closeModal, queryClient }) {
    return (
        <QueryClientProvider client={queryClient}>
            <ModalContent uuid={uuid} setUuid={setUuid} isOpen={isOpen} closeModal={closeModal} queryClient={queryClient} />
        </QueryClientProvider>
    )
}

function ModalContent({ uuid, setUuid, isOpen, closeModal, queryClient }) {
    const classes = useStyles()
    const { register, formState: { errors }, handleSubmit, reset, control } = useForm();
    const [isReadOnly, setReadOnly] = useState(false)
    const { isLoading, data } = useQuery(["task", uuid], () => uuid && readTask(uuid))

    const getHandleErrors = (response) => {
        const { data } = response
        for (const key in data) {
            errors[key] = data[key][0]
        }
    }

    useEffect(() => {
        if (uuid && data && data.data) {
            setReadOnly(true)
            reset({
                name: data.data.name,
                text: data.data.text,
                plan_end_at: data.data.plan_end_at
            })
        } else if (!uuid) {
            reset({})
            setReadOnly(false)
        }

    }, [uuid, data, reset])

    const mutateCreateTask = useMutation(createTask, {
        onSuccess: ({ data }) => {
            setUuid(data.uuid)
            ToastsStore.success('Задача создана!')
            queryClient.invalidateQueries("task")
        },
        onError: ({ response }) => getHandleErrors(response)
    })

    const addTask = data => {
        mutateCreateTask.mutate({
            assignee,
            plan_end_at: new Date(data.plan_end_at).toISOString(),
            ...data
        })
    }

    const done = useMutation(() => doneTask(uuid), {
        onSuccess: ({ data }) => {
            setUuid(data.uuid)
            setReadOnly(true)
            ToastsStore.success('Задача завершена!')
            queryClient.invalidateQueries("gg")
            queryClient.invalidateQueries("tasks")
            queryClient.invalidateQueries("task")
        },
        onError: ({ response }) => getHandleErrors(response)
    })    
    
    const handleDoneTask = data => done.mutate({
        assignee,
        plan_end_at: new Date(data.plan_end_at).toISOString(),
        ...data
    })

    const save = useMutation((data) => saveTask(uuid, data), {
        onSuccess: ({ data }) => {
            setUuid(data.uuid)
            setReadOnly(true)
            ToastsStore.success('Задача успешно сохранена!')
            queryClient.invalidateQueries("task")
        },
        onError: ({ response }) => getHandleErrors(response)
    })    
    
    const handleSaveTask = data => save.mutate({
        assignee,
        plan_end_at: new Date(data.plan_end_at).toISOString(),
        ...data
    })

    const setTitle = (status) => {
        switch (status) {
            case 'DONE':
                return 'Задача завершена'

            case 'PLAN':
                return 'Задача'

            default:
                return 'Новая задача'
        }
    } 

    return (
        <Modal
            open={isOpen}
            onClose={closeModal}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            className={classes.modal}
        >
            {isLoading ? <Box>loading</Box> :
                <Box className={classes.content}>
                    <Box className={classes.titleBlock}>
                        <Typography variant="h6">
                            {setTitle(data.data && data.data.status)}
                        </Typography>
                        <CloseIcon className={classes.closeIcon} onClick={closeModal} />
                    </Box>
                    <Box className={classes.container}>
                        <Controller
                            render={({ field }) => 
                                <TextField {...field}
                                    error={!!errors.name}
                                    helperText={errors.name}
                                    id="name" 
                                    type="text"
                                    label="Название задачи" 
                                    variant="outlined" 
                                    className={classes.input}
                                    InputProps={{
                                        readOnly: isReadOnly
                                    }} 
                                />
                            }
                            control={control}
                            {...register("name")}
                         />
                        <Controller
                            render={({ field }) => 
                                <TextField {...field}
                                    error={!!errors.text}
                                    helperText={errors.text}
                                    id="text" 
                                    type="text"
                                    label="Название задачи" 
                                    variant="outlined" 
                                    className={classes.input}
                                    InputProps={{
                                        readOnly: isReadOnly
                                    }}
                                />
                            }
                             control={control}
                             {...register("text")} 
                        />
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Controller
                                render={({ field }) => 
                                    <KeyboardDateTimePicker {...field}
                                        autoOk
                                        id="plan_end_at"
                                        variant="inline"
                                        ampm={false}
                                        inputVariant="outlined"
                                        label="Дата завершения"
                                        format="dd.MM.yyyy HH:mm"
                                        className={classes.input}
                                        readOnly={isReadOnly}
                                    />
                                }
                                name="plan_end_at"
                                control={control}
                                {...register("plan_end_at")} 
                                defaultValue={null}
                            />
                        </MuiPickersUtilsProvider>
                        <Box className={classes.actionsBlock}>
                            {Object.keys(data.data ? data.data : {}).length > 0 ?
                                <>
                                    <Box>
                                        <Button 
                                            variant="contained" 
                                            color="primary" 
                                            size="small" 
                                            disabled={isReadOnly}
                                            onClick={handleSubmit(handleSaveTask)}
                                        >
                                            Сохранить
                                        </Button>
                                        {data.data && data.data.status === "PLAN" &&
                                            <Button 
                                                variant="contained" 
                                                color="primary" 
                                                size="small" 
                                                style={{ marginLeft: '20px' }}
                                                onClick={handleSubmit(handleDoneTask)}
                                            >
                                                Завершить
                                            </Button>
                                        }
                                    </Box>   
                                    <MenuComponent readOnlyFalse={() => setReadOnly(false)} />
                                </>
                            : <Button 
                                variant="contained" 
                                color="primary" 
                                size="small" 
                                onClick={handleSubmit(addTask)}
                            >
                                Сохранить
                            </Button>}
                        </Box>
                    </Box>
                </Box>
            }
        </Modal>
    )
}

export default memo(App)