
import DataController from './components/DataController'
import Modal from './components/Modal'
import MUIDataTable from "mui-datatables"
import useDebounce from '../../../../../../utils/debounce'
import { AppBar, Toolbar, Typography, Box, Tabs, Tab, Button } from '@material-ui/core';
import { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { getTasks } from '../../../../../../api/privatAPI'
import { QueryClient, QueryClientProvider, useQuery } from 'react-query'

const useStyles = makeStyles(() => ({
    root: {
        width: '100%'
    },
    box: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: '0 10px'
    },
    toolBar: {
        justifyContent: 'space-between',
        background: 'white',
        color: 'black'
    },
    input: {
        width: 280
    },
    btn: {
        textTransform: 'none',
    },
    content: {
        padding: '35px 30px', 
        background: 'lightgray', 
        minHeight: 'calc(100vh - 180px)'
    }
}))

const queryClient = new QueryClient()

function App() {
    return (
        <QueryClientProvider client={queryClient}>
            <Tasks />
        </QueryClientProvider>
    )
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

function Tasks() {
    const classes = useStyles()
    const [uuid, setUuid] = useState('')
    const [list, setList] = useState([])
    const [value, setValue] = useState(0)
    const [searchValue, setSearchValue] = useState('')
    const [fromDate, setFromDate] = useState(null)
    const [toDate, setToDate] = useState(null)
    const [isOpen, setIsOpen] = useState(false)
    const limit = 10
    const [offset, setOffset] = useState(0)

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

	const debounceSearch = useDebounce(searchValue);
    const { isLoading, error, data } = useQuery(["tasks", debounceSearch, fromDate, toDate, value, limit, offset], 
        () => getTasks(
                debounceSearch, 
                fromDate ? new Date(fromDate).toISOString() : '', 
                toDate ? new Date(toDate).toISOString() : '', 
                value ? 'DONE' : 'PLAN', 
                limit, 
                offset * 10
            )
    )

    useEffect(() => {
        if (data && data.data && data.data.results) {
            setList(data.data.results.reduce((prev, item) => {
                return [
                    ...prev,
                    [
                        item.name,
                        item.created_at,
                        '-',
                        item.assignee ? item.assignee.first_name + ' ' + item.assignee.last_name : '-',
                        item.patient ? item.patient.first_name + ' ' + item.patient.last_name : '-',
                        '-',
                        <Button 
                            color="primary" 
                            className={classes.btn} 
                            onClick={() => {
                                setUuid(item.uuid);
                                setIsOpen(true)
                            }}
                        >
                            Подробнее
                        </Button>
                    ]
                ]
            }, []))
        }
    }, [data, classes.btn])

    const columns = ["Наименование", "Дата создания", "Дата завершения", "Исполнитель", "Пациент", "Приём", "Действия"]

    console.log(data)

    return (
        <Box className={classes.root}>
            <AppBar position="static">
                <Toolbar className={classes.toolBar}>
                    <Typography variant="h6" className={classes.title}>
                        Задачи
                    </Typography>
                    <Typography variant="subtitle1" className={classes.title}>
                        Админов админ
                    </Typography>
                </Toolbar>
            </AppBar>
            <Box className={classes.box}>
                <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
                    <Tab label="Текущие задачи" {...a11yProps(0)} />
                    <Tab label="Завершенные задачи" {...a11yProps(1)} />
                </Tabs>
                <Button variant="contained" color="primary" size="small" onClick={() => setIsOpen(true)}>
                    Добавить задачу
                </Button>
            </Box>

            <Modal 
                uuid={uuid} 
                setUuid={setUuid}
                isOpen={isOpen} 
                closeModal={() => {
                    setUuid('')
                    setIsOpen(false)
                }} 
                queryClient={queryClient}
            />

            <Box className={classes.content}>
                <DataController 
                    searchValue={searchValue}
                    setSearchValue={setSearchValue}
                    fromDate={fromDate} 
                    setFromDate={setFromDate} 
                    toDate={toDate}
                    setToDate={setToDate} 
                />
                {isLoading ? 'Loading...' : error ? error 
                :
                    <MUIDataTable
                        columns={columns}
                        options={{
                            textLabels: {
                                body: {
                                    noMatch: "Данных нет"
                                }
                            },
                            filter: false,
                            search: false,
                            download: false,
                            print: false,
                            viewColumns: false,
                            selectableRowsHideCheckboxes: true,
                            onRowClick: (item) => item[6].props.onClick(),
                            count: data && data.data ? data.data.count : 0,
                            onChangePage: (currentPage) => setOffset(currentPage),
                            page: offset,
                            rowsPerPageOptions: 10,
                            serverSide: true
                        }}
                        data={list}
                    />
                }
            </Box>
        </Box>
    )
}

export default App