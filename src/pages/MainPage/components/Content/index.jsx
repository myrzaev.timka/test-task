import Main from './components/Main';
import Tasks from './components/Tasks';
import { Route } from "react-router-dom";

function Content() {
    return (
        <>
            <Route exact path="/" component={Main} />
            <Route path="/tasks" component={Tasks} />
        </>
    )
}

export default Content