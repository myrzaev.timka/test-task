import { useEffect } from 'react'
import { NavBar, Content } from './components'
import { checkIfTokenValid } from '../../utils/token'
import { useHistory } from 'react-router-dom'

function MainPage() {
    const isTokenValid = checkIfTokenValid()
    const history = useHistory()
    
    useEffect(() => {
        if (!isTokenValid) {
            history.push('/login')
        }
    }, [isTokenValid, history])

    return (
        <div style={{ display: 'flex' }}>
            <NavBar />
            <Content />
        </div>
    )
}

export default MainPage