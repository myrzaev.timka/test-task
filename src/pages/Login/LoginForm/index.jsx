import { useState, useEffect } from 'react'
import { TextField, Button, Paper, Typography } from '@material-ui/core'
import logo from '../../../icons/logo-icon.svg'
import { makeStyles } from '@material-ui/core/styles';
import { useForm } from "react-hook-form"
import { login, getApiErrorMessage } from '../../../api'
import { setToken, checkIfTokenValid } from '../../../utils/token'
import { useHistory } from 'react-router-dom'
import { QueryClient, QueryClientProvider, useMutation } from 'react-query'

const useStyles = makeStyles(() => ({
    form: {
        background: 'white', 
        width: 350, 
        height: 600, 
        padding: '25px 35px',
        borderRadius: 4,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    logo: {
        width: 125, 
        height: 155, 
        marginTop: 35
    },
    title: {
        margin: '50px 0 10px 0'
    },
    subtitle: {
        textAlign: "center", margin: '10px 0'
    },
    input: {
        width: '100%', 
        margin: 10
    },
    error: {
        color: 'red', textAlign: 'center'
    },
    btn: {
        width: 220, marginTop: 20
    }

}));

const queryClient = new QueryClient()

function App() {
    return (
        <QueryClientProvider client={queryClient}>
            <LoginForm />
        </QueryClientProvider>
    )
  }

function LoginForm() {
    const classes = useStyles()
    const [error, setError] = useState('');
    const { register, formState: { errors }, handleSubmit } = useForm();
    const isTokenValid = checkIfTokenValid()
    const history = useHistory()

    const mutation = useMutation(login, {
        onSuccess: ({ data }) => {
            setToken(data.access)
        },
        onError: (response) => {
            setError(getApiErrorMessage(response))
        }
    })

    const onSubmit = data => {
        setError('')
        mutation.mutate(data)
    };

    useEffect(() => {
        if (isTokenValid) {
            history.push('/tasks')
        }
    }, [isTokenValid, history])

    return (
        <QueryClientProvider client={queryClient}>
            <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
                <Paper elevation={0}>
                    <img src={logo} alt="logo" className={classes.logo} />
                </Paper>
                <Typography variant="h5" className={classes.title}>
                    Aibomed
                </Typography>
                <Typography className={classes.subtitle}>
                    Система операционно-учетного <br /> управления клиникой
                </Typography>
                <TextField 
                    error={!!errors.email}
                    id="email" 
                    label="Email" 
                    variant="outlined" 
                    className={classes.input} 
                    {...register("email", { required: true })}
                />
                <TextField 
                    error={!!errors.password}
                    id="password" 
                    type="password"
                    label="Пароль" 
                    variant="outlined" 
                    className={classes.input} 
                    {...register("password", { required: true })}
                />
                {error ? <Typography variant="subtitle3" className={classes.error}>{error}</Typography> : null}
                <Button 
                    type="submit"
                    variant="contained" 
                    className={classes.btn}
                >
                    Войти
                </Button>
            </form>
        </QueryClientProvider>
    )
}

export default App