import { Grid } from '@material-ui/core'
import LoginForm from './LoginForm'

function Login() {
    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            style={{ minHeight: '100vh', background: 'lightgray' }}
        >
            <Grid item>
                <LoginForm />
            </Grid>   
        </Grid> 
    )
}

export default Login