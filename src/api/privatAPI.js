import axios from 'axios'
import { endpoints } from './endpoints';
import { getToken } from '../utils/token'

export const privateAPI = axios.create({
    headers: { 'content-type': 'application/json' },
})

privateAPI.interceptors.request.use(config => {
    config.headers.Authorization = `Bearer ${getToken()}`
    return config
})

export const getTasks = (search, created_at_after, created_at_before, status, limit, offset) => {
    const url = endpoints.tasks(search, created_at_after, created_at_before, status, limit, offset);
  
    return privateAPI.get(url);
}

export const createTask = (data) => {
    const url = endpoints.tasks();
  
    return privateAPI.post(url, data);
}

export const readTask = (uuid) => {
    const url = endpoints.actionsTask(uuid);
  
    return privateAPI.get(url);
}

export const saveTask = (uuid, data) => {
    const url = endpoints.actionsTask(uuid);
  
    return privateAPI.put(url, data);
}

export const doneTask = (uuid, data) => {
    const url = endpoints.doneTask(uuid);
  
    return privateAPI.post(url, data);
}