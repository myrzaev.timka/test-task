export const baseApiURL = 'http://65.21.178.123'
export const assignee = 'ea46530d-1848-46bb-8993-f7c44447e58b'

export const endpoints = {
    login: `${baseApiURL}/api/v1/users/token/`,
    verifyToken: `${baseApiURL}/api/v1/users/token/verify/`,
    tasks: (search, created_at_after, created_at_before, status, limit, offset) => 
        `${baseApiURL}/api/v1/tasks/`
        + (search ? `?search=${search}` : '')
        + (created_at_after ? `${search ? '&' : '?'}created_at_after=${created_at_after}` : '')
        + (created_at_before ? `${search || created_at_after ? '&' : '?'}created_at_before=${created_at_before}` : '')
        + (status ? `${search || created_at_after || created_at_before ? '&' : '?'}status=${status}` : '')
        + (limit ? `&limit=${limit}` : '')
        + (offset ? `&offset=${offset}` : '')
    ,
    actionsTask: uuid => `${baseApiURL}/api/v1/tasks/${uuid}/`,
    doneTask: uuid => `${baseApiURL}/api/v1/tasks/${uuid}/done/`
}