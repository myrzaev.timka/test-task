import * as axios from 'axios';
import { endpoints } from './endpoints';

const axiosInstance = axios.create({
    headers: {
        'content-type': 'application/json'
    }
})

export const getApiErrorMessage = error => {
	return error.response && error.response.data && error.response.data.non_field_errors
		? error.response.data.non_field_errors[0]
		: 'Что-то пошло не так!';
};

export const login = (data) => {
    const url = endpoints.login;
  
    return axiosInstance.post(url, data);
};