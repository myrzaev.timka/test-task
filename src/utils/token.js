import jwtDecode from 'jwt-decode'
export const LOCAL_STORAGE_TOKEN_KEY = 'token'

export const getToken = () => localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY)
export const removeToken = () => localStorage.removeItem(LOCAL_STORAGE_TOKEN_KEY)
export const setToken = (token) => localStorage.setItem(LOCAL_STORAGE_TOKEN_KEY, token)
export const decodeToken = (token) => jwtDecode(token)

export const checkIfTokenValid = () => {
  const token = getToken()
  if (!token) {
    return false
  }
  const user = decodeToken(token)
  return user.exp > new Date().getTime() / 1000
}