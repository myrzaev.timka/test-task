import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { ToastsStore, ToastsContainer, ToastsContainerPosition } from "react-toasts";

ReactDOM.render(
  <React.StrictMode>
    <App />
    <ToastsContainer store={ToastsStore} position={ToastsContainerPosition.BOTTOM_RIGHT} />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
